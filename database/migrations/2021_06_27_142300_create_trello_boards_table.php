<?php

use App\Models\TrelloBoard;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrelloBoardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trello_boards', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->boolean('closed')->default(false);
            $table->string('name', 1024);
            $table->longText('description')->nullable();
            $table->string('bg_color_top')->nullable();
            $table->string('bg_color_bottom')->nullable();
            $table->text('bg_image_url')->nullable();
            $table->timestamps();
        });

        Schema::create('trello_board_user', function (Blueprint $table) {
            $table->foreignIdFor(TrelloBoard::class)->constrained();
            $table->foreignIdFor(User::class)->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trello_board_user');
        Schema::dropIfExists('trello_boards');
    }
}
