<?php

use App\Models\TrelloCard;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrelloChecklistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trello_checklists', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->foreignIdFor(TrelloCard::class)->index();
            $table->string('name', 1024);
            $table->float('position', 12);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trello_checklists');
    }
}
