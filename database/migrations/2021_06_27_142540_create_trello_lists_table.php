<?php

use App\Models\TrelloBoard;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrelloListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trello_lists', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->foreignIdFor(TrelloBoard::class)->index();
            $table->boolean('closed')->default(false);
            $table->string('name', 1024);
            $table->float('position', 12);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trello_lists');
    }
}
