<?php

use App\Models\TrelloChecklist;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrelloChecklistItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trello_checklist_items', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->foreignIdFor(TrelloChecklist::class)->index();
            $table->boolean('complete')->index();
            $table->string('name', 1024);
            $table->float('position')->nullable();
            $table->date('due')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trello_checklist_items');
    }
}
