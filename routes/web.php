<?php

use App\Jobs\Trello\ImportBoards;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Laravel\Socialite\Facades\Socialite;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::name('auth')->get('/auth/redirect', function () {
    return Socialite::driver('trello')->redirect();
});

Route::get('/auth/callback', function () {
    $trelloUser = Socialite::driver('trello')->user();

    $user = User::firstOrNew([
        'trello_id' => $trelloUser->getId(),
    ]);

    if (! $user->id) {
        $user->name = $trelloUser->getName();
        $user->email = $trelloUser->getEmail();
        $user->password = Hash::make(Str::random(24));
    }

    if (! $user->email) {
        $user->email = 'trello+'.$trelloUser->getName().'@example.com';
    }

    $user->trello_token = $trelloUser->token;

    $user->save();

    Auth::login($user);

    ImportBoards::dispatch($user);

    return redirect()->to(route('dashboard'));
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
