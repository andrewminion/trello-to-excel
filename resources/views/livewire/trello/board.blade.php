@php
    $style = 'background: ';
    if ($board->bg_image_url) {
        $style .= 'url("'.$board->bg_image_url.'") no-repeat center center / cover';

        if ($board->bg_color_top && $board->bg_color_bottom) {
            $style .= ', ';
        }
    }

    if ($board->bg_color_top && $board->bg_color_bottom) {
        $style .= 'linear-gradient('.$board->bg_color_top.', '.$board->bg_color_bottom.')';
    }

    $style .= '; text-shadow: 0 0 3px rgba(0,0,0,0.5)';
@endphp

<div
    wire:click="export"
    wire:loading.class="opacity-30"
    class="h-48 py-4 px-4 text-white text-lg font-bold cursor-pointer hover:opacity-75 active:opacity-50 transition-all"
    style="{{ $style }}"
    wire:key="{{ $board->id }}"
>
    {{ $board->name }}
</div>
