<div>
    <h2 class="text-xl font-bold py-4 px-4 mt-4 mb-4">{{ __('Click a Board to Export as Excel') }}</h2>

    <p class="m-4">
        <a
            class="inline-flex items-center px-4 py-2 bg-green-600 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-green-800 active:bg-green-900 disabled:opacity-25 transition cursor-pointer"
            wire:click="refreshBoards()"
        >
            {{ __('Refresh Boards') }}
        </a>
    </p>

    <h3 class="text-lg font-bold py-4 px-4 mt-4 mb-4">{{ __('Open Boards') }}</h3>

    <div class="grid grid-cols-4 gap-4 px-4 pb-4" wire:target="refreshBoards" wire:loading.class="hidden">
        @foreach ($openBoards as $board)
            @livewire('trello.board', ['board' => $board], key($board->id))
        @endforeach
    </div>

    <h3 class="text-lg font-bold py-4 px-4 mt-4 mb-4">{{ __('Closed Boards') }}</h3>

    <div class="grid grid-cols-4 gap-4 px-4 pb-4" wire:target="refreshBoards" wire:loading.class="hidden">
        @foreach ($closedBoards as $board)
            @livewire('trello.board', ['board' => $board], key($board->id))
        @endforeach
    </div>
</div>
