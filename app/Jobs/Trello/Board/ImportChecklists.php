<?php

namespace App\Jobs\Trello\Board;

use App\Models\TrelloBoard;
use App\Models\TrelloChecklist;
use App\Models\TrelloChecklistItem;
use App\Models\TrelloList;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class ImportChecklists implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public TrelloBoard $trelloBoard;
    public User $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(TrelloBoard $trelloBoard, User $user = null)
    {
        $this->trelloBoard = $trelloBoard;

        if (! $user) {
            $user = Auth::user();
        }
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return \Illuminate\Support\Collection|\App\Models\TrelloChecklist[]
     */
    public function handle()
    {
        $checklists = Http::get('https://api.trello.com/1/boards/'.$this->trelloBoard->id.'/checklists', [
            'key' => config('services.trello.client_id'),
            'token' => $this->user->trello_token,
        ]);

        return collect($checklists->json())
            ->map(function ($checklistData) {
                $checklist = TrelloChecklist::firstOrNew([
                    'id' => $checklistData['id'],
                ]);
                $checklist->fill([
                    'trello_card_id' => $checklistData['idCard'],
                    'name' => $checklistData['name'],
                    'position' => $checklistData['pos'],
                ]);
                $checklist->save();

                collect($checklistData['checkItems'])
                    ->each(function ($checklistItemData) {
                        $checklistItem = TrelloChecklistItem::firstOrNew([
                            'id' => $checklistItemData['id'],
                        ]);
                        $checklistItem->fill([
                            'trello_checklist_id' => $checklistItemData['idChecklist'],
                            'name' => $checklistItemData['name'],
                            'complete' => $checklistItemData['state'] === 'complete' ? true : false,
                            'position' => $checklistItemData['pos'],
                            'due' => $checklistItemData['due'],
                        ]);
                        $checklistItem->save();
                    });

                return $checklist;
            });
    }
}
