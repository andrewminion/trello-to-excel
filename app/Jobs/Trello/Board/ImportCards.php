<?php

namespace App\Jobs\Trello\Board;

use App\Models\TrelloBoard;
use App\Models\TrelloCard;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class ImportCards implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public TrelloBoard $trelloBoard;
    public User $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(TrelloBoard $trelloBoard, User $user = null)
    {
        $this->trelloBoard = $trelloBoard;

        if (! $user) {
            $user = Auth::user();
        }
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return \Illuminate\Support\Collection|\App\Models\TrelloChecklist[]
     */
    public function handle()
    {
        $cards = Http::get('https://api.trello.com/1/boards/'.$this->trelloBoard->id.'/cards', [
            'key' => config('services.trello.client_id'),
            'token' => $this->user->trello_token,
        ]);

        return collect($cards->json())
            ->map(function ($cardData) {
                $card = TrelloCard::firstOrNew([
                    'id' => $cardData['id'],
                ]);
                $card->fill([
                    'trello_list_id' => $cardData['idList'],
                    'name' => $cardData['name'],
                    'description' => $cardData['desc'],
                    'position' => $cardData['pos'],
                ]);
                $card->save();

                return $card;
            });
    }
}
