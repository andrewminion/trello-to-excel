<?php

namespace App\Jobs\Trello\Board;

use App\Models\TrelloBoard;
use App\Models\TrelloChecklist;
use App\Models\TrelloChecklistItem;
use App\Models\TrelloList;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class ImportLists implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public TrelloBoard $trelloBoard;
    public User $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(TrelloBoard $trelloBoard, User $user = null)
    {
        $this->trelloBoard = $trelloBoard;

        if (! $user) {
            $user = Auth::user();
        }
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return \Illuminate\Support\Collection|\App\Models\TrelloChecklist[]
     */
    public function handle()
    {
        $lists = Http::get('https://api.trello.com/1/boards/'.$this->trelloBoard->id.'/lists', [
            'key' => config('services.trello.client_id'),
            'token' => $this->user->trello_token,
        ]);

        return collect($lists->json())
            ->map(function ($listData) {
                $list = TrelloList::firstOrNew([
                    'id' => $listData['id'],
                ]);
                $list->fill([
                    'trello_board_id' => $listData['idBoard'],
                    'name' => $listData['name'],
                    'position' => $listData['pos'],
                    'closed' => $listData['closed'],
                ]);
                $list->save();

                return $list;
            });
    }
}
