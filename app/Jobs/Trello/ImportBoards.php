<?php

namespace App\Jobs\Trello;

use App\Models\TrelloBoard;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class ImportBoards implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public User $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user = null)
    {
        if (! $user) {
            $user = Auth::user();
        }

        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return \Illuminate\Support\Collection|\App\Models\TrelloBoard[]
     */
    public function handle()
    {
        $trelloBoards = Http::get('https://api.trello.com/1/members/'.$this->user->trello_id.'/boards', [
            'key' => config('services.trello.client_id'),
            'token' => $this->user->trello_token,
        ]);

        return collect($trelloBoards->json())
            ->map(function ($trelloBoard) {
                /** @var TrelloBoard */
                $board = TrelloBoard::firstOrNew([
                    'id' => $trelloBoard['id'],
                ]);

                $board->closed = $trelloBoard['closed'];
                $board->name = $trelloBoard['name'];
                $board->description = $trelloBoard['desc'];

                $board->bg_color_top = $trelloBoard['prefs']['backgroundTopColor'];
                $board->bg_color_bottom = $trelloBoard['prefs']['backgroundBottomColor'];
                $board->bg_image_url = collect($trelloBoard['prefs']['backgroundImageScaled'])
                    ->filter(function ($image) {
                        return 256 === $image['width'];
                    })
                    ->first()['url'] ?? $trelloBoard['prefs']['backgroundImage'];

                $board->save();

                if (! $board->users->contains($this->user)) {
                    $board->users()->attach($this->user->id);
                }

                return $board;
            });
    }
}
