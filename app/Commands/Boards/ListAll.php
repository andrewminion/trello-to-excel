<?php

namespace App\Commands\Boards;

use App\Models\TrelloBoard;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Console\Command;;

class ListAll extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'board:List';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'List all boards';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->table(
            ['ID', 'Name'],
            TrelloBoard::query()->select('id', 'name')->get()->toArray(),
        );
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
