<?php

namespace App\Commands\Board;

use App\Models\TrelloBoard;
use App\Models\TrelloCard;
use App\Models\TrelloChecklist;
use App\Models\TrelloChecklistItem;
use App\Models\TrelloList;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Console\Command;;

class Import extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'board:import {file : JSON file to import}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Import an entire Trello board to the database';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = json_decode(file_get_contents($this->argument('file')));

        $board = TrelloBoard::firstOrNew([
            'id' => $data->id,
        ]);
        $board->fill([
            'name' => $data->name,
            'description' => $data->desc,
        ]);
        $board->save();
        $this->info('Imported the '.$board->name.' board');

        collect($data->lists)
            ->each(function ($listData) {
                $list = TrelloList::firstOrNew([
                    'id' => $listData->id,
                ]);
                $list->fill([
                    'trello_board_id' => $listData->idBoard,
                    'name' => $listData->name,
                    'position' => $listData->pos,
                    'closed' => $listData->closed,
                ]);
                $list->save();
            });
        $this->info('Imported '.count($data->lists).' lists');

        collect($data->cards)
            ->each(function ($cardData) {
                $card = TrelloCard::firstOrNew([
                    'id' => $cardData->id,
                ]);
                $card->fill([
                    'trello_list_id' => $cardData->idList,
                    'name' => $cardData->name,
                    'description' => $cardData->desc,
                    'position' => $cardData->pos,
                ]);
                $card->save();
            });
        $this->info('Imported '.count($data->cards).' cards');

        collect($data->checklists)
            ->each(function ($checklistData) {
                $checklist = TrelloChecklist::firstOrNew([
                    'id' => $checklistData->id,
                ]);
                $checklist->fill([
                    'trello_card_id' => $checklistData->idCard,
                    'name' => $checklistData->name,
                    'position' => $checklistData->pos,
                ]);
                $checklist->save();

                collect($checklistData->checkItems)
                    ->each(function ($checklistItemData) {
                        $checklistItem = TrelloChecklistItem::firstOrNew([
                            'id' => $checklistItemData->id,
                        ]);
                        $checklistItem->fill([
                            'trello_checklist_id' => $checklistItemData->idChecklist,
                            'name' => $checklistItemData->name,
                            'complete' => $checklistItemData->state === 'complete' ? true : false,
                            'position' => $checklistItemData->pos,
                            'due' => $checklistItemData->due,
                        ]);
                        $checklistItem->save();
                    });
            });
        $this->info('Imported '.count($data->checklists).' checklists with '.TrelloChecklistItem::count().' items');
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
