<?php

namespace App\Commands\Board;

use App\Exports\BoardExport;
use App\Models\TrelloBoard;
use Illuminate\Console\Command;
use Illuminate\Console\Scheduling\Schedule;
use Maatwebsite\Excel\Facades\Excel;

class Export extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'board:export
                                    {id : Trello board ID}
                                    {--filename= : Export filename}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Export a board to an Excel sheet';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $board = TrelloBoard::where('id', $this->argument('id'))->sole();

        if ($this->option('filename')) {
            $filename = $this->option('filename');
        } else {
            $filename = $board->name.'.xlsx';
        }

        Excel::store(new BoardExport($board->id), $filename);
        $this->info('Saved results to '.storage_path($filename));
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
