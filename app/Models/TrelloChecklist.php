<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TrelloChecklist
 *
 * @property string $id
 * @property string $trello_card_id
 * @property string $name
 * @property float $position
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\TrelloCard $card
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TrelloChecklistItem[] $checklist_items
 * @property-read int|null $checklist_items_count
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloChecklist newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloChecklist newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloChecklist query()
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloChecklist whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloChecklist whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloChecklist whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloChecklist wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloChecklist whereTrelloCardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloChecklist whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TrelloChecklist extends Model
{
    use HasFactory;

    protected $keyType = 'string';
    public $incrementing = false;

    protected $fillable = [
        'id',
        'trello_card_id',
        'name',
        'position',
    ];

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\App\Models\TrelloCard */
    public function card()
    {
        return $this->belongsTo(TrelloCard::class);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\HasMany|\App\Models\TrelloChecklistItem[] */
    public function checklist_items()
    {
        return $this->hasMany(TrelloChecklistItem::class);
    }
}
