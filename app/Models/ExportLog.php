<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ExportLog
 *
 * @property int $id
 * @property int $user_id
 * @property string $trello_board_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\TrelloBoard $trello_board
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|ExportLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ExportLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ExportLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|ExportLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExportLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExportLog whereTrelloBoardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExportLog whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExportLog whereUserId($value)
 * @mixin \Eloquent
 */
class ExportLog extends Model
{
    use HasFactory;

    protected $fillable = [
        'trello_board_id',
        'user_id',
    ];

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\App\Models\TrelloBoard */
    public function trello_board()
    {
        return $this->belongsTo(TrelloBoard::class);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\App\Models\User */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
