<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TrelloCard
 *
 * @property string $id
 * @property string $trello_list_id
 * @property string $name
 * @property string|null $description
 * @property float $position
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TrelloChecklistItem[] $checklist_items
 * @property-read int|null $checklist_items_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TrelloChecklist[] $checklists
 * @property-read int|null $checklists_count
 * @property-read \App\Models\TrelloList $list
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloCard newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloCard newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloCard query()
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloCard whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloCard whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloCard whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloCard whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloCard wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloCard whereTrelloListId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloCard whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TrelloCard extends Model
{
    use HasFactory;

    protected $keyType = 'string';
    public $incrementing = false;

    protected $fillable = [
        'id',
        'trello_list_id',
        'name',
        'description',
        'position',
    ];

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\App\Models\TrelloList */
    public function list()
    {
        return $this->belongsTo(TrelloList::class);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\HasMany|\App\Models\TrelloChecklist[] */
    public function checklists()
    {
        return $this->hasMany(TrelloChecklist::class);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\HasManyThrough|\App\Models\TrelloChecklistItem[] */
    public function checklist_items()
    {
        return $this->hasManyThrough(TrelloChecklistItem::class, TrelloChecklist::class);
    }
}
