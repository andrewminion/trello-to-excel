<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TrelloList
 *
 * @property string $id
 * @property string $trello_board_id
 * @property bool $closed
 * @property string $name
 * @property float $position
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\TrelloBoard $board
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TrelloCard[] $cards
 * @property-read int|null $cards_count
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloList newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloList newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloList query()
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloList whereClosed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloList whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloList whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloList whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloList wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloList whereTrelloBoardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloList whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TrelloList extends Model
{
    use HasFactory;

    protected $keyType = 'string';
    public $incrementing = false;

    protected $casts = [
        'closed' => 'boolean',
    ];

    protected $fillable = [
        'id',
        'trello_board_id',
        'name',
        'position',
        'closed',
    ];

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\App\Models\TrelloBoard */
    public function board()
    {
        return $this->belongsTo(TrelloBoard::class);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\HasMany|\App\Models\TrelloCard[] */
    public function cards()
    {
        return $this->hasMany(TrelloCard::class);
    }
}
