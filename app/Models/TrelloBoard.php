<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TrelloBoard
 *
 * @property string $id
 * @property int $closed
 * @property string $name
 * @property string|null $description
 * @property string|null $bg_color_top
 * @property string|null $bg_color_bottom
 * @property string|null $bg_image_url
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TrelloCard[] $cards
 * @property-read int|null $cards_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ExportLog[] $export_logs
 * @property-read int|null $export_logs_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TrelloList[] $lists
 * @property-read int|null $lists_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloBoard newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloBoard newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloBoard query()
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloBoard whereBgColorBottom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloBoard whereBgColorTop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloBoard whereBgImageUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloBoard whereClosed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloBoard whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloBoard whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloBoard whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloBoard whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloBoard whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TrelloBoard extends Model
{
    use HasFactory;

    protected $keyType = 'string';
    public $incrementing = false;

    protected $fillable = [
        'id',
        'user_id',
        'name',
        'description',
    ];


    /** @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\App\Models\User[] */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\HasMany|\App\Models\TrelloList[] */
    public function lists()
    {
        return $this->hasMany(TrelloList::class);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\HasManyThrough|\App\Models\TrelloCard[] */
    public function cards()
    {
        return $this->hasManyThrough(TrelloCard::class, TrelloList::class);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\HasMany|\App\Models\ExportLog[] */
    public function export_logs()
    {
        return $this->hasMany(ExportLog::class);
    }
}
