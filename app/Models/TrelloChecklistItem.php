<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TrelloChecklistItem
 *
 * @property string $id
 * @property string $trello_checklist_id
 * @property bool $complete
 * @property string $name
 * @property float|null $position
 * @property \Illuminate\Support\Carbon|null $due
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\TrelloChecklist $checklist
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloChecklistItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloChecklistItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloChecklistItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloChecklistItem whereComplete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloChecklistItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloChecklistItem whereDue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloChecklistItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloChecklistItem whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloChecklistItem wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloChecklistItem whereTrelloChecklistId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloChecklistItem whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TrelloChecklistItem extends Model
{
    use HasFactory;

    protected $keyType = 'string';
    public $incrementing = false;

    protected $casts = [
        'complete' => 'boolean',
        'due' => 'date',
    ];

    protected $fillable = [
        'id',
        'trello_checklist_id',
        'name',
        'complete',
        'position',
        'due',
    ];

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\App\Models\TrelloCheckList */
    public function checklist()
    {
        return $this->belongsTo(TrelloCheckList::class);
    }
}
