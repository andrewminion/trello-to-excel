<?php

namespace App\Exports;

use App\Models\TrelloBoard;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class BoardExport implements WithMultipleSheets
{
    use Exportable;

    protected TrelloBoard $board;

    public function __construct(string $id)
    {
        $this->board = TrelloBoard::where('id', $id)->sole();
    }

    public function sheets() : array
    {
        $sheets = $this
            ->board
            ->lists
            ->map(function ($list) {
                return new ListExport($list);
            });

        return $sheets->toArray();
    }
}
