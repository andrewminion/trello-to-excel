<?php

namespace App\Exports;

use App\Models\TrelloCard;
use App\Models\TrelloChecklist;
use App\Models\TrelloChecklistItem;
use App\Models\TrelloList;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;

class ListExport implements
    FromCollection,
    WithHeadings,
    WithMapping,
    WithTitle
{
    protected TrelloList $list;

    public function __construct(TrelloList $list)
    {
        $this->list = $list;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return $this
            ->list
            ->cards;
    }

    public function headings(): array
    {
        return [
            'Name',
            'Complete',
            'Items',
            'Due',
        ];
    }

    public function map($card): array
    {
        $results = collect([
                [
                    $card->name,
                ],
            ])
            ->merge(
                $card->checklists->flatMap(function (TrelloChecklist $checklist) {
                    return $checklist->checklist_items->map(function (TrelloChecklistItem $checklistItem) {
                        return [
                            null,
                            $checklistItem->complete ? 'yes' : 'no',
                            $checklistItem->name,
                            optional($checklistItem->due)->format('m/d/Y'),
                        ];
                    });
                })
            );

        return $results
            ->toArray();
    }

    public function title() : string
    {
        return $this->list->name;
    }
}
