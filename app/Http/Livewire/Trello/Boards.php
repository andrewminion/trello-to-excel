<?php

namespace App\Http\Livewire\Trello;

use App\Exports\BoardExport;
use App\Jobs\Trello\Board\ImportCards;
use App\Jobs\Trello\Board\ImportChecklists;
use App\Jobs\Trello\Board\ImportLists;
use App\Jobs\Trello\ImportBoards;
use App\Models\ExportLog;
use App\Models\TrelloBoard;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Maatwebsite\Excel\Facades\Excel;

class Boards extends Component
{
    public $openBoards;
    public $closedBoards;

    public function mount()
    {
        $this->refreshBoards();
    }

    public function refreshBoards()
    {
        (new ImportBoards())->handle();

        /** @var User */
        $user = Auth::user();

        $this->openBoards = $user->trello_boards()->where('closed', false)->get();
        $this->closedBoards = $user->trello_boards()->where('closed', true)->get();
    }

    public function render()
    {
        return view('livewire.trello.boards');
    }
}
