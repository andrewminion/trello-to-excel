<?php

namespace App\Http\Livewire\Trello;

use App\Exports\BoardExport;
use App\Jobs\Trello\Board\ImportCards;
use App\Jobs\Trello\Board\ImportChecklists;
use App\Jobs\Trello\Board\ImportLists;
use App\Models\ExportLog;
use App\Models\TrelloBoard;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Maatwebsite\Excel\Facades\Excel;

class Board extends Component
{
    public TrelloBoard $board;

    public function render()
    {
        return view('livewire.trello.board');
    }

    public function export()
    {
        (new ImportLists($this->board))->handle();
        (new ImportCards($this->board))->handle();
        (new ImportChecklists($this->board))->handle();

        (new ExportLog([
            'trello_board_id' => $this->board->id,
            'user_id' => Auth::user()->id,
        ]))->save();

        $name = Str::replace(
            ['/', '\\'],
            ['-', '-'],
            $this->board->name
        );

        return Excel::download(new BoardExport($this->board->id), $name.'.xlsx');
    }
}
