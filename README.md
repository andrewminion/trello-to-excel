# Trello to Excel

This is a simple command-line app to convert a Trello board to an Excel spreadsheet.

![Example](resources/example-screenshot.png)

## Usage

0. Clone this project and run `composer install`
1. Export the board as JSON
1. Run `php trello-to-excel board:import {filename}` where `{filename}` is the path to the JSON export
1. Run `php trello-to-excel board:list` to show all board names/IDs
1. Run `php trello-to-excel board:export {id} {filename}` where `{id}` is the (required) board ID and `{filename}` is an optional filename (if not specified, the file will be named with the board name)
